# -*- coding: utf-8 -*-
"""
Created on Sun Jan  2 17:51:50 2022

@author: lucie
"""


import numpy as np
import random as rd
import matplotlib.pyplot as plt
from sklearn import datasets



#chargement de base de données iris
iris = datasets.load_iris()

'''
# on choisit les colonnes informant sur les pétales
X_iris = iris.data[:,[2,3]]
Y_iris = iris.target
print(iris)
'''

 #On choisit les colonnes informant sur les sépales
X_iris = iris.data[:,[0,1]]
Y_iris = iris.target


# On choisit k = 3  puisque initialement il y a trois types de fleurs
N = len(X_iris)

x=[]
y = []
for i in range(N):
    x.append(X_iris[i][0])
    y.append(X_iris[i][1])
    
print(x)
#Requires : Deux points
#Assigns Renvoie la distance entre ces deux points

def dist(k,o) :
    d = np.sqrt(abs(k[0]-o[0])**2+abs(k[1]-o[1])**2)
    return d


   

#on choisit les trois cluster aléatoirement


c1 = rd.randint(0,N-1)
c2 = rd.randint(0,N-1)
c3 = rd.randint(0,N-1)

#tant que deux points sont égaux, on refait un choix aléatoire
while(c1==c2 or c1==c3 or c2==c3) : 
    c1 = rd.randint(0,N-1)
    c2 = rd.randint(0,N-1)
    c3 = rd.randint(0,N-1)

print("c1- Premier cluster de coorodnnées (", X_iris[c1][0], X_iris[c1][1],")")
print("c2- Deuxième cluster de coorodnnées (", X_iris[c2][0], X_iris[c2][1],")")
print("c3- Troisième cluster de coorodnnées (", X_iris[c3][0], X_iris[c3][1],")")


#affichage du premier graphique avec les clusters marqués en rouge
plt.scatter(x, y)
'''
plt.title('Iris initial')
plt.xlabel('petal lenght')
plt.ylabel('petal Width')
'''

'''
plt.title('Iris initial')
plt.xlabel('sepal lenght')
plt.ylabel('sepal Width')
plt.scatter(X_iris[c1][0], X_iris[c1][1], c='red', label = 'c1')
plt.scatter(X_iris[c2][0], X_iris[c2][1], c='red', label = 'c2')
plt.scatter(X_iris[c3][0], X_iris[c3][1], c='red', label = 'c3')
plt.plot()
plt.show()
'''




#initialisation des nouveaux clusters
c1_nouveau = [0,0]
c2_nouveau = [0,0]
c3_nouveau = [0,0]
cluster_1 = []
cluster_2=[]
cluster_3=[]

#pour chaque point, on calcule la distance pour l'assigner à un cluster
for i in range(N):
        d1 = dist([X_iris[c1][0], X_iris[c1][1]], [X_iris[i][0], X_iris[i][1]])
        d2 = dist([X_iris[c2][0], X_iris[c2][1]],[X_iris[i][0], X_iris[i][1]])
        d3 = dist([X_iris[c3][0], X_iris[c3][1]],[X_iris[i][0], X_iris[i][1]])
        if (d1<d2 and d1<d3) :
            print("Le point",i,"rejoint le premier cluster")
            cluster_1.append(i)
            plt.scatter(X_iris[i][0],X_iris[i][1], c='red', label='C1')
        elif (d1<d2 and d1>d3):
            print("Le point",i,"rejoint le troisième cluster")
            cluster_3.append(i)
            plt.scatter(X_iris[i][0],X_iris[i][1], c='blue', label='C3')

        else :
            cluster_2.append(i)
            plt.scatter(X_iris[i][0],X_iris[i][1], c='green', label='C2')
            
 # on affiche le résultat           
plt.plot()
plt.pause(0.05)
plt.show
n1=len(cluster_1)
n2=len(cluster_2)
n3=len(cluster_3)

#pour chaque cluster, on cherche le nouveau centre 
for i in cluster_1 :
        c1_nouveau[0] = c1_nouveau[0]+X_iris[i][0]
        c1_nouveau[1] = c1_nouveau[1]+X_iris[i][1]
for i in cluster_2 :
        c2_nouveau[0] = c2_nouveau[0]+X_iris[i][0]
        c2_nouveau[1] = c2_nouveau[1]+X_iris[i][1]
for i in cluster_3 :
        c3_nouveau[0] = c3_nouveau[0]+X_iris[i][0]
        c3_nouveau[1] = c3_nouveau[1]+X_iris[i][1]
c1_nouveau[0]=c1_nouveau[0]/n1
c1_nouveau[1]=c1_nouveau[1]/n1
c2_nouveau[0]=c2_nouveau[0]/n2
c2_nouveau[1]=c2_nouveau[1]/n2    
c3_nouveau[0]=c3_nouveau[0]/n3
c3_nouveau[1]=c3_nouveau[1]/n3    

print("c1,c2,c3 avant while")
print(c1_nouveau,c2_nouveau,c3_nouveau) 

# p nous donne le nombre d'itérations    
p=1

#on refait le même algotihme tant que les nouveaux centres ne sont pas égaux aux anciens
while (c1 != c1_nouveau or c2!=c2_nouveau or c3!=c3_nouveau) :
    p=p+1
    c1=c1_nouveau
    c2=c2_nouveau
    c3=c3_nouveau
    c1_nouveau = [0,0]
    c2_nouveau = [0,0]
    c3_nouveau = [0,0]
    cluster_1 = []
    cluster_2=[]
    cluster_3=[]
    for i in range(N):
        d1 = dist(c1, [X_iris[i][0], X_iris[i][1]])
        d2= dist(c2,[X_iris[i][0], X_iris[i][1]])
        d3 = dist(c3,[X_iris[i][0], X_iris[i][1]])
        if (d1<d2 and d1<d3) :
            print("Le point",i,"rejoint le premier cluster")
            cluster_1.append(i)
            plt.scatter(X_iris[i][0],X_iris[i][1], c='red', label='C1')
        elif (d1<d2 and d1>d3):
            print("Le point",i,"rejoint le troisième cluster")
            cluster_3.append(i)
            plt.scatter(X_iris[i][0],X_iris[i][1], c='blue', label='C3')

        else :
            cluster_2.append(i)
            plt.scatter(X_iris[i][0],X_iris[i][1], c='green', label='C2')
    
    '''plt.title('Cluster ')
    plt.xlabel('petal lenght')
    plt.ylabel('petal Width')
    '''
    plt.title('Cluster ')
    plt.xlabel('sepal lenght')
    plt.ylabel('sepal Width')
    plt.plot()
    plt.pause(0.05)
    plt.show
    n1=len(cluster_1)
    n2=len(cluster_2)
    n3=len(cluster_3)
    for i in cluster_1 :
        c1_nouveau[0] = c1_nouveau[0]+X_iris[i][0]
        c1_nouveau[1] = c1_nouveau[1]+X_iris[i][1]
    for i in cluster_2 :
        c2_nouveau[0] = c2_nouveau[0]+X_iris[i][0]
        c2_nouveau[1] = c2_nouveau[1]+X_iris[i][1]
    for i in cluster_3 :
        c3_nouveau[0] = c3_nouveau[0]+X_iris[i][0]
        c3_nouveau[1] = c3_nouveau[1]+X_iris[i][1]
    c1_nouveau[0]=c1_nouveau[0]/n1
    c1_nouveau[1]=c1_nouveau[1]/n1
    c2_nouveau[0]=c2_nouveau[0]/n2
    c2_nouveau[1]=c2_nouveau[1]/n2    
    c3_nouveau[0]=c3_nouveau[0]/n3
    c3_nouveau[1]=c3_nouveau[1]/n3  
print("résultat après", p, "itérations")      