## K-means clustering on Iris dataset
Seeing how the K-means algorithm works on the Iris dataset.
I coded the K-means algorithm myself for this project. 


## The Iris dataset
The Iris dataset is a very well-known dataset that has been used since Ronald Fisher presented it in 1936. It contains the measurements for the height and the width of the petals and the sepals on 150 iris samples across three diffreent varieties: Iris setosa, Iris versicolor, and Iris virginica.
By comparating the differences in petal sizes and sepal sizes, we can find the three varieties of irises from scratch.

## Author
Lucie BECHU
