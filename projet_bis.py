# -*- coding: utf-8 -*-
"""
Created on Tue Jan 18 11:25:45 2022

@author: lucie BECHU
"""



import numpy as np
import random as rd
import matplotlib.pyplot as plt
from sklearn import datasets



#chargement de base de données iris
iris = datasets.load_iris()
X_iris = iris.data

# On choisit k = 3  puisque initialement il y a trois types de fleurs

#tailles du dataset
N = len(X_iris)


#On crée les différentes colonnes pour appliquer K-means
x_sepal = []
y_sepal = []
x_petal = []
y_petal = []
for i in range(N):
    x_sepal.append(X_iris[i][0])
    y_sepal.append(X_iris[i][1])
    x_petal.append(X_iris[i][2])
    y_petal.append(X_iris[i][3])



#Requires : Deux points
#Assigns Renvoie la distance entre ces deux points en utilisant leurs quatre coordonnées

def dist(k,o) :
    d = np.sqrt(abs(k[0]-o[0])**2+abs(k[1]-o[1])**2+abs(k[2]-o[2])**2+abs(k[3]-o[3])**2)
    return d


#on choisit les trois premiers centroïds aléatoirement
c1 = rd.randint(0,N-1)
c2 = rd.randint(0,N-1)
c3 = rd.randint(0,N-1)

#tant que deux points sont égaux, on refait un choix aléatoire
while(c1==c2 or c1==c3 or c2==c3) : 
    c1 = rd.randint(0,N-1)
    c2 = rd.randint(0,N-1)
    c3 = rd.randint(0,N-1)

print("c1- Premier cluster de coorodnnées (",  X_iris[c1][0], X_iris[c1][1],X_iris[c1][2], X_iris[c1][3],")")
print("c2- Deuxième cluster de coorodnnées (", X_iris[c2][0], X_iris[c2][1],X_iris[c2][2], X_iris[c2][3],")")
print("c3- Troisième cluster de coorodnnées (",X_iris[c3][0], X_iris[c3][1],X_iris[c3][2], X_iris[c3][3],")")



#Création de la figure permettant d'afficher les graphiques en fonction des différents paramètres
figure, axis = plt.subplots(2, 3)
  

#premier graphique en haut à gauche
axis[0, 0].scatter(x_sepal, y_sepal,s= 2)
axis[0, 0].scatter(X_iris[c1][0], X_iris[c1][1], c='red', label = 'c1',s= 2.5)
axis[0, 0].scatter(X_iris[c2][0], X_iris[c2][1], c='red', label = 'c2',s= 2.5)
axis[0, 0].scatter(X_iris[c3][0], X_iris[c3][1], c='red', label = 'c3',s= 2.5)

#deuxième graphique en haut
axis[0, 1].scatter(x_sepal, x_petal,s= 2)
axis[0, 1].scatter(X_iris[c1][0], X_iris[c1][2], c='red', label = 'c1',s= 2.5)
axis[0, 1].scatter(X_iris[c2][0], X_iris[c2][2], c='red', label = 'c2',s= 2.5)
axis[0, 1].scatter(X_iris[c3][0], X_iris[c3][2], c='red', label = 'c3',s= 2.5)

#Troisième graphique en haut
axis[0, 2].scatter(x_sepal, y_petal, s= 2) 
axis[0, 2].scatter(X_iris[c1][0], X_iris[c1][3], c='red', label = 'c1',s= 2.5)
axis[0, 2].scatter(X_iris[c2][0], X_iris[c2][3], c='red', label = 'c2',s= 2.5)
axis[0, 2].scatter(X_iris[c3][0], X_iris[c3][3], c='red', label = 'c3',s= 2.5)


#Premier graphqiue en bas
axis[1, 0].scatter(x_petal, y_sepal,s= 2)
axis[1, 0].scatter(X_iris[c1][2], X_iris[c1][1], c='red', label = 'c1',s= 2.5)
axis[1, 0].scatter(X_iris[c2][2], X_iris[c2][1], c='red', label = 'c2',s= 2.5)
axis[1, 0].scatter(X_iris[c3][2], X_iris[c3][1], c='red', label = 'c3',s= 2.5)

#deuxième graphique en bas
axis[1, 1].scatter(y_petal, y_sepal,s= 2)
axis[1, 1].scatter(X_iris[c1][3], X_iris[c1][1], c='red', label = 'c1',s= 2.5)
axis[1, 1].scatter(X_iris[c2][3], X_iris[c2][1], c='red', label = 'c2',s= 2.5)
axis[1, 1].scatter(X_iris[c3][3], X_iris[c3][1], c='red', label = 'c3',s= 2.5)

#troisième graphique en bas
axis[1, 2].scatter(x_petal, y_petal, s= 2) 
axis[1, 2].scatter(X_iris[c1][2], X_iris[c1][3], c='red', label = 'c1',s= 2.5)
axis[1, 2].scatter(X_iris[c2][2], X_iris[c2][3], c='red', label = 'c2',s= 2.5)
axis[1, 2].scatter(X_iris[c3][2], X_iris[c3][3], c='red', label = 'c3',s= 2.5)


#Nom des différents graphiques
axis[0, 0].set_title("y_sepal=f(x_sepal)")
axis[0, 1].set_title("x_petal=f(x_sepal)")
axis[0, 2].set_title("y_petal= f(x_sepal)")
axis[1, 0].set_title("y_sepal=f(x_petal)")
axis[1, 1].set_title("y_sepal=f(y_petal)")
axis[1, 2].set_title("y_petal= f(x_sepal)")
plt.show()






#initialisation des nouveaux clusters
c1_nouveau = [0,0,0,0]
c2_nouveau = [0,0,0,0]
c3_nouveau = [0,0,0,0]
cluster_1 = []
cluster_2 = []
cluster_3 = []

figure, axis = plt.subplots(2, 3)
#pour chaque point, on calcule la distance pour l'assigner à un cluster et afficher ce point dans la bonne couleur dans les différents graphiques
for i in range(N):
        d1 = dist([X_iris[c1][0], X_iris[c1][1], X_iris[c1][2], X_iris[c1][3]], [X_iris[i][0], X_iris[i][1], X_iris[i][2], X_iris[i][3]])
        d2 = dist([X_iris[c2][0], X_iris[c2][1],X_iris[c2][2], X_iris[c2][3]],[X_iris[i][0], X_iris[i][1], X_iris[i][2], X_iris[i][3]])
        d3 = dist([X_iris[c3][0], X_iris[c3][1],X_iris[c3][2], X_iris[c3][3]],[X_iris[i][0], X_iris[i][1], X_iris[i][2], X_iris[i][3]])
        if (d1<d2 and d1<d3) :
            print("Le point",i,"rejoint le premier cluster")
            cluster_1.append(i)
            axis[0, 0].scatter(X_iris[i][0],X_iris[i][1], c='red', label='C1',s=2)
            axis[0, 1].scatter(X_iris[i][0],X_iris[i][2], c='red', label='C1',s=2)
            axis[0, 2].scatter(X_iris[i][0],X_iris[i][3], c='red', label='C1',s=2)
            axis[1, 0].scatter(X_iris[i][2],X_iris[i][1], c='red', label='C1',s=2)
            axis[1, 1].scatter(X_iris[i][3],X_iris[i][1], c='red', label='C1',s=2)
            axis[1, 2].scatter(X_iris[i][2],X_iris[i][3], c='red', label='C1',s=2)
        elif (d1<d2 and d1>d3):
            print("Le point",i,"rejoint le troisième cluster")
            cluster_3.append(i)
            axis[0, 0].scatter(X_iris[i][0],X_iris[i][1], c='blue', label='C3',s=2)
            axis[0, 1].scatter(X_iris[i][0],X_iris[i][2], c='blue', label='C3',s=2)
            axis[0, 2].scatter(X_iris[i][0],X_iris[i][3], c='blue', label='C3',s=2)
            axis[1, 0].scatter(X_iris[i][2],X_iris[i][1], c='blue', label='C3',s=2)
            axis[1, 1].scatter(X_iris[i][3],X_iris[i][1], c='blue', label='C3',s=2)
            axis[1, 2].scatter(X_iris[i][2],X_iris[i][3], c='blue', label='C3',s=2)

        else :
            cluster_2.append(i)
            axis[0, 0].scatter(X_iris[i][0],X_iris[i][1], c='green', label='C2',s=2)
            axis[0, 1].scatter(X_iris[i][0],X_iris[i][2], c='green', label='C2',s=2)
            axis[0, 2].scatter(X_iris[i][0],X_iris[i][3], c='green', label='C2',s=2)
            axis[1, 0].scatter(X_iris[i][2],X_iris[i][1], c='green', label='C2',s=2)
            axis[1, 1].scatter(X_iris[i][3],X_iris[i][1], c='green', label='C2',s=2)
            axis[1, 2].scatter(X_iris[i][2],X_iris[i][3], c='green', label='C2',s=2)
            
            
# on affiche le résultat           

axis[0, 0].set_title("y_sepal=f(x_sepal)")
axis[0, 1].set_title("x_petal=f(x_sepal)")
axis[0, 2].set_title("y_petal= f(x_sepal)")
axis[1, 0].set_title("y_sepal=f(x_petal)")
axis[1, 1].set_title("y_sepal=f(y_petal)")
axis[1, 2].set_title("y_petal= f(x_sepal)")
plt.pause(0.05)
plt.show()
n1=len(cluster_1)
n2=len(cluster_2)
n3=len(cluster_3)

#pour chaque cluster, on cherche le nouveau centre 
for i in cluster_1 :
        c1_nouveau[0] = c1_nouveau[0]+X_iris[i][0]
        c1_nouveau[1] = c1_nouveau[1]+X_iris[i][1]
        c1_nouveau[2] = c1_nouveau[2]+X_iris[i][2]
        c1_nouveau[3] = c1_nouveau[3]+X_iris[i][3]
        
        
for i in cluster_2 :
        c2_nouveau[0] = c2_nouveau[0]+X_iris[i][0]
        c2_nouveau[1] = c2_nouveau[1]+X_iris[i][1]
        c2_nouveau[2] = c2_nouveau[2]+X_iris[i][2]
        c2_nouveau[3] = c2_nouveau[3]+X_iris[i][3]
        
        
for i in cluster_3 :
        c3_nouveau[0] = c3_nouveau[0]+X_iris[i][0]
        c3_nouveau[1] = c3_nouveau[1]+X_iris[i][1]
        c3_nouveau[2] = c3_nouveau[2]+X_iris[i][2]
        c3_nouveau[3] = c3_nouveau[3]+X_iris[i][3]
        
        
# Calcul des nouveaux coordonnées des centroids
        
c1_nouveau[0]=c1_nouveau[0]/n1
c1_nouveau[1]=c1_nouveau[1]/n1
c1_nouveau[2]=c1_nouveau[2]/n1
c1_nouveau[3]=c1_nouveau[3]/n1

c2_nouveau[0]=c2_nouveau[0]/n2
c2_nouveau[1]=c2_nouveau[1]/n2
c2_nouveau[2]=c2_nouveau[2]/n2
c2_nouveau[3]=c2_nouveau[3]/n2
    
c3_nouveau[0]=c3_nouveau[0]/n3
c3_nouveau[1]=c3_nouveau[1]/n3 
c3_nouveau[2]=c3_nouveau[2]/n3
c3_nouveau[3]=c3_nouveau[3]/n3    

print("c1,c2,c3 avant while")
print(c1_nouveau,c2_nouveau,c3_nouveau) 


# p nous donne le nombre d'itérations    
p=1

#on refait le même algotihme tant que les nouveaux centres ne sont pas égaux aux anciens
while (c1 != c1_nouveau or c2!=c2_nouveau or c3!=c3_nouveau) :
    figure, axis = plt.subplots(2, 3)
    p=p+1
    c1=c1_nouveau
    c2=c2_nouveau
    c3=c3_nouveau
    c1_nouveau = [0,0,0,0]
    c2_nouveau = [0,0,0,0]
    c3_nouveau = [0,0,0,0]
    cluster_1 = []
    cluster_2 = []
    cluster_3 = []
    for i in range(N):
        d1 = dist(c1, [X_iris[i][0], X_iris[i][1],X_iris[i][2], X_iris[i][3]])
        d2 = dist(c2, [X_iris[i][0], X_iris[i][1],X_iris[i][2], X_iris[i][3]])
        d3 = dist(c3, [X_iris[i][0], X_iris[i][1],X_iris[i][2], X_iris[i][3]])
        if (d1<d2 and d1<d3) :
            print("Le point",i,"rejoint le premier cluster")
            cluster_1.append(i)
            axis[0, 0].scatter(X_iris[i][0],X_iris[i][1], c='red', label='C1',s=2)
            axis[0, 1].scatter(X_iris[i][0],X_iris[i][2], c='red', label='C1',s=2)
            axis[0, 2].scatter(X_iris[i][0],X_iris[i][3], c='red', label='C1',s=2)
            axis[1, 0].scatter(X_iris[i][2],X_iris[i][1], c='red', label='C1',s=2)
            axis[1, 1].scatter(X_iris[i][3],X_iris[i][1], c='red', label='C1',s=2)
            axis[1, 2].scatter(X_iris[i][2],X_iris[i][3], c='red', label='C1',s=2)
        elif (d1<d2 and d1>d3):
            print("Le point",i,"rejoint le troisième cluster")
            cluster_3.append(i)
            
            axis[0, 0].scatter(X_iris[i][0],X_iris[i][1], c='blue', label='C3',s=2)
            axis[0, 1].scatter(X_iris[i][0],X_iris[i][2], c='blue', label='C3',s=2)
            axis[0, 2].scatter(X_iris[i][0],X_iris[i][3], c='blue', label='C3',s=2)
            axis[1, 0].scatter(X_iris[i][2],X_iris[i][1], c='blue', label='C3',s=2)
            axis[1, 1].scatter(X_iris[i][3],X_iris[i][1], c='blue', label='C3',s=2)
            axis[1, 2].scatter(X_iris[i][2],X_iris[i][3], c='blue', label='C3',s=2)

        else :
            cluster_2.append(i)
            axis[0, 0].scatter(X_iris[i][0],X_iris[i][1], c='green', label='C2',s=2)
            axis[0, 1].scatter(X_iris[i][0],X_iris[i][2], c='green', label='C2',s=2)
            axis[0, 2].scatter(X_iris[i][0],X_iris[i][3], c='green', label='C2',s=2)
            axis[1, 0].scatter(X_iris[i][2],X_iris[i][1], c='green', label='C2',s=2)
            axis[1, 1].scatter(X_iris[i][3],X_iris[i][1], c='green', label='C2',s=2)
            axis[1, 2].scatter(X_iris[i][2],X_iris[i][3], c='green', label='C2',s=2)
    
    axis[0, 0].set_title("y_sepal=f(x_sepal)")
    axis[0, 1].set_title("x_petal=f(x_sepal)")
    axis[0, 2].set_title("y_petal= f(x_sepal)")
    axis[1, 0].set_title("y_sepal=f(x_petal)")
    axis[1, 1].set_title("y_sepal=f(y_petal)")
    axis[1, 2].set_title("y_petal= f(x_sepal)")
    plt.pause(1)
    plt.show
    n1=len(cluster_1)
    n2=len(cluster_2)
    n3=len(cluster_3)
    if (n1 == 0 or n2 == 0 or n3 == 0) :
        print('Erreur, il faut relancer le programme')
        break
    for i in cluster_1 :
        c1_nouveau[0] = c1_nouveau[0]+X_iris[i][0]
        c1_nouveau[1] = c1_nouveau[1]+X_iris[i][1]
        c1_nouveau[2] = c1_nouveau[2]+X_iris[i][2]
        c1_nouveau[3] = c1_nouveau[3]+X_iris[i][3]
        
    for i in cluster_2 :
        c2_nouveau[0] = c2_nouveau[0]+X_iris[i][0]
        c2_nouveau[1] = c2_nouveau[1]+X_iris[i][1]
        c2_nouveau[2] = c2_nouveau[2]+X_iris[i][2]
        c2_nouveau[3] = c2_nouveau[3]+X_iris[i][3]
        
    for i in cluster_3 :
        c3_nouveau[0] = c3_nouveau[0]+X_iris[i][0]
        c3_nouveau[1] = c3_nouveau[1]+X_iris[i][1]
        c3_nouveau[2] = c3_nouveau[2]+X_iris[i][2]
        c3_nouveau[3] = c3_nouveau[3]+X_iris[i][3]
        
        
    # Calcul des nouveaux coordonnées des centroids
        
    c1_nouveau[0]=c1_nouveau[0]/n1
    c1_nouveau[1]=c1_nouveau[1]/n1
    c1_nouveau[2]=c1_nouveau[2]/n1
    c1_nouveau[3]=c1_nouveau[3]/n1

    c2_nouveau[0]=c2_nouveau[0]/n2
    c2_nouveau[1]=c2_nouveau[1]/n2
    c2_nouveau[2]=c2_nouveau[2]/n2
    c2_nouveau[3]=c2_nouveau[3]/n2
    
    c3_nouveau[0]=c3_nouveau[0]/n3
    c3_nouveau[1]=c3_nouveau[1]/n3 
    c3_nouveau[2]=c3_nouveau[2]/n3
    c3_nouveau[3]=c3_nouveau[3]/n3  
print("résultat après", p, "itérations")      